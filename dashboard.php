<?php
	session_start();
	if($_SESSION['status_login'] != true){
		echo '<script>window.location="login.php"</script>';
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewsport" content="width=device-width, initial-scale=1">
	<title>SISTEM AKADEMIK POLMED</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<!-- header -->
	<header>
		<div class="container">
			<h1><a href="dashboard.php">POLITEKNIK NEGERI MEDAN</a></h1>
			<ul>
				<li><a href="dashboard.php">Dashboard</a></li>
				<li><a href="profil.php">Profil</a></li>
				<li><a href="prodi.php">Prodi</a></li>
				<li><a href="tabelmhs.php">Tabel Mahasiswa</a></li>
				<li><a href="keluar.php">Keluar</a></li>
			</ul>
		</div>
	</header>
	
	<!-- content -->
	<div class="section">
		<div class="container">
			<h2>Selamat Datang di website resmi Politeknik Negeri Medan</h2>
			<marquee behavior="alternatif" bgcolor="white">
				<img src="gambar/gambar.jpg"height="200px" width="250px">
			</marquee>
			<div class="box">
				<h4>Ini adalah website resmi Politeknik Negeri Medan. Semoga dengan adanya website ini 
				segala informasi mengenai mahasiswa dapat tersampaikan dengan <i>up-to-date dan terpercaya.</h4>
				<br/>
				<br/>
				<p>Sebagai salah satu perguruan tinggi negeri di Sumatera Utara, Politeknik Negeri Medan saat ini 
				menjadi pusat pendidikan vokasi yang berfokus pada pengembangan kemampuan Sumber Daya Manusia dan 
				memiliki visi global untuk turut serta meningkatkan angka partisipasi kasar pendidikan di Indonesia</p>
				<br/>
				<br/>
				<p>Politeknik Negeri Medan secara aktif juga terlibat dalam berbagai penelitian terapan.
				Selain itu para dosen Politeknik Negeri Medan juga ikut serta dalam berbagai kegiatan pengabdian 
				masyarakat sebagai rasa tanggung jawab sosial. Selain itu dalam rangka pengembangan jaringan, 
				Politeknik Negeri Medan juga melakukan berbagai kerjasama dengan berbagai pihak baik regional maupun internasional.</p>
			</div>
			<img src="https://lh3.googleusercontent.com/-hER-IkvuW5Y/TjpTDguanRI/AAAAAAAAAJQ/fvl8DapmRjs/banner%2525203.gif">
		</div>
	</div>
	
	<!-- bagian footer -->
	<footer>
		<div class="container">
			<medium>Copyright &copy; 2021 - SISTEM AKADEMIK POLITEKNIK NEGERI MEDAN.<medium>
			<p>Dibuat dan dirancang oleh Dicky Kurniawan Syahputra.</a></p>
			<p>&copy; 2021 Hak Cipta Dilindungi.</a></p>
		</div>
	</footer>	
</body>
</html>