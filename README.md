## Tampilan Website

1. **Login**

![Img 1](screenshot/Login.JPG)

2. **Dashboard**

![Img 2](screenshot/HalamanDashboard.JPG)

3. **Profil**

![Img 3](screenshot/Profile.JPG)

4. **Prodi**

![Img 4](screenshot/Prodi.JPG)

5. **Daftar Mahasiswa**

![Img 5](screenshot/TabelMahasiswa.JPG)
