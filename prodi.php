<?php
	session_start();
	include 'db.php';
	if($_SESSION['status_login'] != true){
		echo '<script>window.location="login.php"</script>';
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewsport" content="width=device-width, initial-scale=1">
	<title>SISTEM AKADEMI POLMED</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<!-- header -->
	<header>
		<div class="container">
			<h1><a href="dashboard.php">SISTEM AKADEMI POLMED</a></h1>
			<ul>
				<li><a href="dashboard.php">Dashboard</a></li>
				<li><a href="profil.php">Profil</a></li>
				<li><a href="prodi.php">Prodi</a></li>
				<li><a href="tabelmhs.php">Tabel Mahasiswa</a></li>
				<li><a href="keluar.php">Keluar</a></li>
			</ul>
		</div>
	</header>
	
	<!-- content -->
	<div class="section">
		<div class="container">
			<h3>Prodi</h3>
			<div class="box">
				<p><a href="tambah-prodi.php">Tambah Data Prodi</a></p>
				<table border="1" cellspacing="0" class="table">
					<thead>
						<tr>
							<th width="60px">No</th>
							<th>Prodi</th>
							<th width="150px">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$no = 1;
							$prodi = mysqli_query($conn, "SELECT * FROM tb_prodi ORDER BY prodi_id DESC");
							if(mysqli_num_rows($prodi) > 0){
							while($row = mysqli_fetch_array($prodi)){
						?>
						<tr>
							<td><?php echo $no++ ?></td>
							<td><?php echo $row['prodi_name'] ?></td>
							<td>
								<a href="edit-prodi.php?id=<?php echo $row['prodi_id'] ?>">Edit</a> || <a href="
								proses-hapus.php?idk=<?php echo $row['prodi_id'] ?>" onclick="return confirm('Yakin ingin hapus ?'
								)">Hapus</a>
							</td>
						</tr>
						<?php }}else{ ?>
							<tr>
								<td colspan="3">Tidak ada data</td>
							</tr>
						<?php }?>
					</tbody>
				</table>	
			</div>
		</div>
	</div>
</body>
</html>