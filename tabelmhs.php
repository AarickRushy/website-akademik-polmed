<?php
	session_start();
	include 'db.php';
	if($_SESSION['status_login'] != true){
		echo '<script>window.location="login.php"</script>';
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewsport" content="width=device-width, initial-scale=1">
	<title>SISTEM AKADEMI POLMED</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<!-- header -->
	<header>
		<div class="container">
			<h1><a href="dashboard.php">SISTEM AKADEMI POLMED</a></h1>
			<ul>
				<li><a href="dashboard.php">Dashboard</a></li>
				<li><a href="profil.php">Profil</a></li>
				<li><a href="prodi.php">Prodi</a></li>
				<li><a href="tabelmhs.php">Tabel Mahasiswa</a></li>
				<li><a href="keluar.php">Keluar</a></li>
			</ul>
		</div>
	</header>
	
	<!-- content -->
	<div class="section">
		<div class="container">
			<h1>Tabel Mahasiswa</h1>
			<div class="box">
				<p><a href="tambah-mhs.php">Tambah Mahasiswa</a></p>
				<table border="1" cellspacing="0" class="table">
					<thead>
						<tr>
							<th width="60px">No</th>
							<th>Prodi</th>
							<th>Nama Mahasiswa</th>
							<th>NIM</th>
							<th>Alamat</th>
							<th>Status</th>
							<th width="150px">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$no = 1;
							$mhs = mysqli_query($conn, "SELECT * FROM tb_mhs LEFT JOIN tb_prodi USING (prodi_id)
							ORDER BY mhs_id DESC");
							if(mysqli_num_rows($mhs) > 0){
							while($row = mysqli_fetch_array($mhs)){
						?>
						<tr>
							<td><?php echo $no++ ?></td>
							<td><?php echo $row['prodi_name'] ?></td>
							<td><?php echo $row['mhs_name'] ?></td>
							<td><?php echo $row['mhs_nim'] ?></td>
							<td><?php echo $row['alamat_mhs'] ?></td>
							<td><?php echo ($row['mhs_status'] == 0)? 'Tidak Aktif':'Aktif'; ?></td>
							<td>
								<a href="edit-mhs.php?id=<?php echo $row['mhs_id'] ?>">Edit</a> || <a href="
								proses-hapus.php?idp=<?php echo $row['mhs_id'] ?>" onclick="return confirm('Yakin ingin hapus ?'
								)">Hapus</a>
							</td>
						</tr>
						<?php }}else{ ?>
							<tr>
								<td colspan="8">Tidak ada data</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>	
			</div>
		</div>
	</div>
</html>