<?php
	session_start();
	include 'db.php';
	if($_SESSION['status_login'] != true){
		echo '<script>window.location="login.php"</script>';
	}
	
	$query = mysqli_query($conn, "SELECT * FROM tb_admin WHERE admin_id = '".$_SESSION['id']."' ");
	$d = mysqli_fetch_object($query);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewsport" content="width=device-width, initial-scale=1">
	<title>SISTEM AKADEMI POLMED</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<!-- header -->
	<header>
		<div class="container">
			<h1><a href="dashboard.php">SISTEM AKADEMI POLMED</a></h1>
			<ul>
				<li><a href="dashboard.php">Dashboard</a></li>
				<li><a href="profil.php">Profil</a></li>
				<li><a href="prodi.php">Prodi</a></li>
				<li><a href="tabelmhs.php">Tabel Mahasiswa</a></li>
				<li><a href="keluar.php">Keluar</a></li>
			</ul>
		</div>
	</header>
	
	<!-- content -->
	<div class="section">
		<div class="container">
			<h3>Profil</h3>
			<div class="box">
				<form action="" method="POST">
					<input type="text" name="nama" placeholder="Nama Lengkap" class="input-control" value="<?php echo $d->admin_name ?>"required>
					<input type="text" name="user" placeholder="Username" class="input-control" value="<?php echo $d->username ?>"required>
					<input type="text" name="hp" placeholder="No Hp" class="input-control" value="<?php echo $d->admin_telp ?>"required>
					<input type="text" name="email" placeholder="Email" class="input-control" value="<?php echo $d->admin_email ?>"required>
					<input type="text" name="alamat" placeholder="Alamat" class="input-control" value="<?php echo $d->admin_address ?>"required>
					<input type="submit" name="submit" value="Ubah Profile" class="btn">
				</form>
				<?php
					if(isset($_POST['submit'])){
						
						$nama	 = $_POST['nama'];
						$user 	 = $_POST['user'];
						$hp 	 = $_POST['hp'];
						$email 	 = $_POST['email'];
						$alamat  = $_POST['alamat'];
						
						$update	 = mysqli_query($conn, "UPDATE tb_admin SET 
										admin_name = '".$nama."',
										username = '".$user."',
										admin_telp = '".$hp."',
										admin_email = '".$email."',
										admin_address = '".$alamat."'
										WHERE admin_id = '".$d->admin_id."' ");
						if($update){
							echo '<script>alert("Ubah data berhasil")</script>';
							echo '<script>window.location="profil.php"</script>';
						}else{
							echo 'gagal '.mysqli_error($conn);
						}
					}
				?>
			</div>
		</div>
	</div>
	
	<!-- bagian footer -->
	<footer>         
		<div class="container">
			<medium>Copyright &copy; 2021 - SISTEM AKADEMIK POLITEKNIK NEGERI MEDAN.<medium>
			<p>Dibuat dan dirancang oleh Dicky Kurniawa Syahputa.</a></p>
			<p>&copy; 2021 Hak Cipta Dilindungi.</a></p>
		</div>
	</footer>	
</body>
</html>
</body>
</html>