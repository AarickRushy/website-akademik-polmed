<?php
	session_start();
	include 'db.php';
	if($_SESSION['status_login'] != true){
		echo '<script>window.location="login.php"</script>';
	}
	
	$prodi = mysqli_query($conn, "SELECT * FROM tb_prodi WHERE prodi_id = '".$_GET['id']."' ");
	$p = mysqli_fetch_object($prodi);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewsport" content="width=device-width, initial-scale=1">
	<title>SISTEM AKADEMIK POLMED</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<!-- header -->
	<header>
		<div class="container">
			<h1><a href="dashboard.php">SISTEM AKADEMIK POLMED</a></h1>
			<ul>
				<li><a href="dashboard.php">Dashboard</a></li>
				<li><a href="profil.php">Profil</a></li>
				<li><a href="prodi.php">Prodi</a></li>
				<li><a href="tabelmhs.php">Tabel Mahasiswa</a></li>
				<li><a href="keluar.php">Keluar</a></li>
			</ul>
		</div>
	</header>
	
	<!-- content -->
	<div class="section">
		<div class="container">
			<h3>Edit Prodi</h3>
			<div class="box">
				<form action="" method="POST">
					<input type="text" name="nama" placeholder="Nama Prodi" class="input-control" value="<?php echo 
					$p->prodi_name ?>" required>
					<input type="submit" name="submit" value="Submit" class="btn">
				</form>
				<?php
					if(isset($_POST['submit'])){
						
						$nama = ucwords($_POST['nama']);
						
						$update = mysqli_query($conn, "UPDATE tb_prodi SET
												prodi_name = '".$nama."'
												WHERE prodi_id = '".$p->prodi_id."' ");
												
						if($update){
							echo '<script>alert("Edit data berhasil")</script>';
							echo '<script>window.location="prodi.php"</script>';
						}else{
							echo 'gagal '.mysqli_error($conn);
						}
					}
				?>
			</div>
		</div>
	</div>
</body>
</html>