<?php
	session_start();
	include 'db.php';
	if($_SESSION['status_login'] != true){
		echo '<script>window.location="login.php"</script>';
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewsport" content="width=device-width, initial-scale=1">
	<title>SISTEM AKADEMI POLMED</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
</head>
<body>
	<!-- header -->
	<header>
		<div class="container">
			<h1><a href="dashboard.php">SISTEM AKADEMI POLMED</a></h1>
			<ul>
				<li><a href="dashboard.php">Dashboard</a></li>
				<li><a href="profil.php">Profil</a></li>
				<li><a href="prodi.php">Prodi</a></li>
				<li><a href="tabelmhs.php">Tabel Mahasiswa</a></li>
				<li><a href="keluar.php">Keluar</a></li>
			</ul>
		</div>
	</header>
	
	<!-- content -->
	<div class="section">
		<div class="container">
			<h3>Tambah Mahasiswa</h3>
			<div class="box">
				<form action="" method="POST" enctype="multipart/form-data">
					<select class="input-control" name="prodi" required>
						<option value="">--Pilih Prodi--</option>
						<?php
							$prodi = mysqli_query($conn, "SELECT * FROM tb_prodi ORDER BY prodi_id DESC");
							while($p = mysqli_fetch_array($prodi)){
						?>
						<option value="<?php echo $p['prodi_id'] ?>"><?php echo $p['prodi_name'] ?></option>
						<?php } ?>
					</select>
					
					<input type="text" name="nama" class="input-control" placeholder="Nama Mahasiswa" required>
					<input type="text" name="nim" class="input-control" placeholder="NIM" required>
					<textarea class="input-control" name="alamat" placeholder="Alamat Mahasiswa"></textarea><br>
					<select class="input-control" name="status">
						<option value="">--Pilih--</option>
						<option value="1">Aktif</option>
						<option value="0">Tidak Aktif</option>
					</select>
					<input type="submit" name="submit" value="Submit" class="btn">
				</form>
				<?php
					if(isset($_POST['submit'])){
						
						//print_r($_FILES['gambar']);
						//menampung inputan dari form
						$prodi = $_POST['prodi'];
						$nama = $_POST['nama'];
						$nim = $_POST['nim'];
						$alamat = $_POST['alamat'];
						$status = $_POST['status'];	
							
							$insert = mysqli_query($conn, "INSERT INTO tb_mhs VALUES (
										null,
										'".$prodi."',
										'".$nama."',
										'".$nim."',
										'".$alamat."',
										'".$status."',
										null
											) ");
							if($insert){
								echo '<script>alert("Tambah data berhasil")</script>';
								echo '<script>window.location="tabelmhs.php"</script>';
							}else{
								echo 'gagal '.mysqli_error($conn);
							}
						}
				?>
			</div>
		</div>
	</div>
	<script>
		CKEDITOR.replace( 'alamat' );
    </script>
</body>
</html>